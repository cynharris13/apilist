package com.example.apilist.model.entity

/**
 * A p i list.
 *
 * @property apis
 * @constructor Create empty A p i list
 */
data class APIList(
    val apis: List<String> = emptyList()
)
