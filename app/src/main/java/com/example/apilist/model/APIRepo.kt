package com.example.apilist.model

import com.example.apilist.model.dto.APIResponse
import com.example.apilist.model.entity.APIList
import com.example.apilist.model.remote.APIService
import javax.inject.Inject

/**
 * A p i repo.
 *
 * @property apiService
 * @constructor Create empty A p i repo
 */
class APIRepo @Inject constructor(private val apiService: APIService) {
    /**
     * Get categories.
     *
     * @return
     */
    suspend fun getCategories(): APIList {
        val response = apiService.getCategories()
        return if (response.isSuccessful) {
            val apis = response.body() ?: APIResponse()
            APIList(apis = apis.categories)
        } else { APIList() }
    }
}
