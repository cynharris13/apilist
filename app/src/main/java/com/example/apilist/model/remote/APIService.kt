package com.example.apilist.model.remote

import com.example.apilist.model.dto.APIResponse
import retrofit2.Response
import retrofit2.http.GET

/**
 * A p i service.
 *
 * @constructor Create empty A p i service
 */
interface APIService {
    @GET("categories")
    suspend fun getCategories(): Response<APIResponse>
}
