package com.example.apilist.model.dto

@kotlinx.serialization.Serializable
data class APIResponse(
    val categories: List<String> = emptyList(),
    val count: Int = 0
)
