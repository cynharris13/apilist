package com.example.apilist.view.category

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.apilist.databinding.CategoryItemBinding

/**
 * Category adapter.
 *
 * @constructor Create empty Category adapter
 */
class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {
    var apiList: MutableList<String> = mutableListOf()

    /**
     * View holder.
     *
     * @property binding
     * @constructor Create empty View holder
     */
    inner class ViewHolder(private val binding: CategoryItemBinding) : RecyclerView.ViewHolder(binding.root) {
        /**
         * Bind category.
         *
         * @param api
         */
        fun bindCategory(api: String) {
            binding.textView.text = api
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = CategoryItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(inflater)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindCategory(apiList[position])
    }

    override fun getItemCount(): Int = apiList.size

    /**
     * Add items.
     *
     * @param apis
     */
    fun addItems(apis: List<String>) {
        val amount = apiList.size
        apiList.addAll(apis)
        notifyItemRangeInserted(amount, apis.size)
    }
}
