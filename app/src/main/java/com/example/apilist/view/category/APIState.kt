package com.example.apilist.view.category

import com.example.apilist.model.entity.APIList

/**
 * A p i state.
 *
 * @property isLoading
 * @property apis
 * @constructor Create empty A p i state
 */
data class APIState(
    val isLoading: Boolean = false,
    val apis: APIList? = null
)
