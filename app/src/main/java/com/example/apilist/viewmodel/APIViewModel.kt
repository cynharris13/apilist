package com.example.apilist.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.apilist.model.APIRepo
import com.example.apilist.view.category.APIState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * A p i view model.
 *
 * @property apiRepo
 * @constructor Create empty A p i view model
 */
@HiltViewModel
class APIViewModel @Inject constructor(private val apiRepo: APIRepo) : ViewModel() {
    private val _state = MutableLiveData(APIState())
    val state: LiveData<APIState> get() = _state

    /**
     * Get categories.
     *
     */
    fun getCategories() = viewModelScope.launch {
        _state.value = _state.value?.copy(isLoading = true)
        val apiList = apiRepo.getCategories()
        _state.value = _state.value?.copy(isLoading = false, apis = apiList)
    }
}
