package com.example.apilist

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * A p i app.
 *
 * @constructor Create empty A p i app
 */
@HiltAndroidApp
class APIApp : Application()
