package com.example.apilist.model

import com.example.apilist.model.dto.APIResponse
import com.example.apilist.model.entity.APIList
import com.example.apilist.model.remote.APIService
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@OptIn(ExperimentalCoroutinesApi::class)
internal class APIRepoTest {
    private val service = mockk<APIService>()
    private val repo = APIRepo(service)

    @Test
    fun getCategories() = runTest {
        // given
        val dto = APIResponse(categories = listOf("lol"))
        coEvery { service.getCategories().isSuccessful } coAnswers { true }
        coEvery { service.getCategories().body() } coAnswers { dto }
        val expected = APIList(listOf("lol"))

        // when
        val actual = repo.getCategories()

        // then
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun getNoCategories() = runTest {
        // given
        coEvery { service.getCategories().isSuccessful } coAnswers { false }
        val expected = APIList()

        // when
        val actual = repo.getCategories()
        // then
        Assertions.assertEquals(expected, actual)
    }
}
