package com.example.apilist.viewmodel

import com.example.apilist.model.APIRepo
import com.example.apilist.model.entity.APIList
import com.example.apilist.util.CoroutinesTestExtension
import com.example.apilist.util.InstantTaskExecutorExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(InstantTaskExecutorExtension::class)
@ExtendWith(CoroutinesTestExtension::class)
internal class APIViewModelTest {
    private val repo = mockk<APIRepo>()
    private val apiVM = APIViewModel(repo)

    @Test
    fun getCategories() = runTest {
        // given
        val expected = APIList(listOf("lol"))
        coEvery { repo.getCategories() } coAnswers { expected }

        // when
        apiVM.getCategories()

        // then
        Assertions.assertFalse(apiVM.state.value?.isLoading ?: true)
        Assertions.assertEquals(expected, apiVM.state.value?.apis)
    }
}
